#!/bin/bash
# no arguments, display a menu with option for info
# about processes

clear
while [ "$svar" != "9" ]
do
 echo ""
 echo "  1 - hvem er jeg og hva er navet på dette scriptet?" 
echo""
 echo " 2 - hvor lenge er det siden siste boot?"
echo""
 echo " 3 - Hva var gjennomsnittlig load siste minutt??"
echo""
 echo " 4 - Hvor mange prosesser og tråder finnes??"
echo""
echo " 5 - Hvor mange context switch'er fant sted siste sekund?"
echo""
 echo " 6 - Hvor mange interrupts fant sted siste sekund??"
echo""
 echo "  9 - Avslutt dette scriptet"
 echo ""
 echo -n "Velg en funksjon: "
 read -r svar
 echo ""
 case $svar in
  1)clear
    echo "Jeg er $(whoami)"
    read -r
    clear
    ;;
2)clear
date -d "$(</proc/uptime awk '{print $1}') seconds ago"
    read -r
    clear
    ;;
 3)clear
uptime
    read -r
    clear
    ;;
 4)clear
top
    read -r
    clear
    ;;
 5)clear
c1=$(grep ctxt /proc/stat | awk '{print $2}')
sleep 1
c2=$(grep ctxt /proc/stat | awk '{print $2}')
echo "Antall context switch'er siste sekund var $("$c2-$c1")"
    read -r
    clear
    ;;
 6)clear
cat /proc/interrupts 
    read -r
    clear
    ;;


  9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg
    read -r
    clear
    ;;
 esac
done
