#!/bin/bash

if [[ -d $1 ]]; then

fult=$(df -h $1 | tail -n -1|awk '{print $5}')
echo "Partisjonen $1 befinner seg på er $fult full"


filer=$(find $1 -depth -type f | wc -l)
        echo "Det finnes $filer filer"

largest=$(find $1 -depth -type f | tr '\n' '\0' | du -ah --files0-from=- | sort -h | tail -n 1)       # largest file, <fileSize> <fileName>
        echo "Den største er $(echo $largest | awk '{print $2}') som er $(echo $largest | awk '{print $1}') stor."

totalSize=$(find $1 -depth -type f | tr '\n' '\0' | du -a --files0-from=- | awk '{print $1}' | sed ':a;N;$!ba;s/\n/+/g' | bc)


gjennomsnitt=$(($totalSize/$filer))
        echo "Gjennomsnittlig filstørrelse er ca $gjennomsnitt bytes."


hardlinks=$(find $1 -depth -type f -print0 | xargs --null ls -la | awk '{print $2 , $9 }' | sort | tail -n 1)


	echo "Filen $(echo $hardlinks | awk '{print $2}') har flest hardlinks, den har $(echo $hardlinks | awk '{print $1}')."

else
	echo "$1 is not a directory..."
fi
